const express = require('express');
const app = express();
const dotenv = require('dotenv');

const route = require('./src/routes/test');

dotenv.config();

app.use(express.json());

app.use('/api', route);

app.listen(process.env.SERVER_PORT, () => {
  console.log(`Server is running on port ${process.env.SERVER_PORT}!`);
});
